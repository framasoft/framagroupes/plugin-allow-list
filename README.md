# Allow list plugin for Sympa 6.2

This plugin is a renaming of [Steve Shipway’s Whitelist plugin](https://github.com/sshipway/sympa-6.2-plugins/tree/master/whitelist-1.2).

See [INSTALL](INSTALL) for installation instructions.
